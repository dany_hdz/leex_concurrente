// ignore-tidy-filelength

//! Rustdoc's HTML rendering module.
//!
//! This modules contains the bulk of the logic necessary for rendering a
//! rustdoc `clean::Crate` instance to a set of static HTML pages. This
//! rendering process is largely driven by the `format!` syntax extension to
//! perform all I/O into files and streams.
//!
//! The rendering process is largely driven by the `Context` and `Cache`
//! structures. The cache is pre-populated by crawling the crate in question,
//! and then it is shared among the various rendering threads. The cache is meant
//! to be a fairly large structure not implementing `Clone` (because it's shared
//! among threads). The context, however, should be a lightweight structure. This
//! is cloned per-thread and contains information about what is currently being
//! rendered.
//!
//! In order to speed up rendering (mostly because of markdown rendering), the
//! rendering process has been parallelized. This parallelization is only
//! exposed through the `crate` method on the context, and then also from the
//! fact that the shared cache is stored in TLS (and must be accessed as such).
//!
//! In addition to rendering the crate itself, this module is also responsible
//! for creating the corresponding search index and source file renderings.
//! These threads are not parallelized (they haven't been a bottleneck yet), and
//! both occur before the crate is rendered.

use std::borrow::Cow;
use std::cell::{Cell, RefCell};
use std::cmp::Ordering;
use std::collections::{BTreeMap, VecDeque};
use std::default::Default;
use std::error;
use std::fmt::{self, Formatter, Write as FmtWrite};
use std::ffi::OsStr;
use std::fs::{self, File};
use std::io::prelude::*;
use std::io::{self, BufReader};
use std::path::{PathBuf, Path, Component};
use std::str;
use std::sync::Arc;
use std::rc::Rc;

use errors;
use serialize::json::{ToJson, Json, as_json};
use syntax::ast;
use syntax::edition::Edition;
use syntax::feature_gate::UnstableFeatures;
use syntax::print::pprust;
use syntax::source_map::FileName;
use syntax::symbol::{Symbol, sym};
use syntax_pos::hygiene::MacroKind;
use rustc::hir::def_id::DefId;
use rustc::middle::privacy::AccessLevels;
use rustc::middle::stability;
use rustc::hir;
use rustc::util::nodemap::{FxHashMap, FxHashSet};
use rustc_data_structures::flock;

use crate::clean::{self, AttributesExt, Deprecation, GetDefId, SelfTy, Mutability};
use crate::config::RenderOptions;
use crate::docfs::{DocFS, ErrorStorage, PathError};
use crate::doctree;
use crate::html::escape::Escape;
use crate::html::format::{Buffer, PrintWithSpace, print_abi_with_space};
use crate::html::format::{print_generic_bounds, WhereClause, href, print_default_space};
use crate::html::format::{Function};
use crate::html::format::fmt_impl_for_trait_page;
use crate::html::item_type::ItemType;
use crate::html::markdown::{self, Markdown, MarkdownHtml, MarkdownSummaryLine, ErrorCodes, IdMap};
use crate::html::{highlight, layout, static_files};
use crate::html::sources;

use minifier;

#[cfg(test)]
mod tests;

mod cache;

use cache::Cache;
crate use cache::ExternalLocation::{self, *};

/// A pair of name and its optional document.
pub type NameDoc = (String, Option<String>);

crate fn ensure_trailing_slash(v: &str) -> impl fmt::Display + '_ {
    crate::html::format::display_fn(move |f| {
        if !v.ends_with("/") && !v.is_empty() {
            write!(f, "{}/", v)
        } else {
            write!(f, "{}", v)
        }
    })
}

#[derive(Debug)]
pub struct Error {
    pub file: PathBuf,
    pub error: io::Error,
}

impl error::Error for Error {
    fn description(&self) -> &str {
        self.error.description()
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let file = self.file.display().to_string();
        if file.is_empty() {
            write!(f, "{}", self.error)
        } else {
            write!(f, "\"{}\": {}", self.file.display(), self.error)
        }
    }
}

impl PathError for Error {
    fn new<P: AsRef<Path>>(e: io::Error, path: P) -> Error {
        Error {
            file: path.as_ref().to_path_buf(),
            error: e,
        }
    }
}

macro_rules! try_none {
    ($e:expr, $file:expr) => ({
        use std::io;
        match $e {
            Some(e) => e,
            None => return Err(Error::new(io::Error::new(io::ErrorKind::Other, "not found"),
                                          $file))
        }
    })
}

macro_rules! try_err {
    ($e:expr, $file:expr) => ({
        match $e {
            Ok(e) => e,
            Err(e) => return Err(Error::new(e, $file)),
        }
    })
}

/// Major driving force in all rustdoc rendering. This contains information
/// about where in the tree-like hierarchy rendering is occurring and controls
/// how the current page is being rendered.
///
/// It is intended that this context is a lightweight object which can be fairly
/// easily cloned because it is cloned per work-job (about once per item in the
/// rustdoc tree).
#[derive(Clone)]
struct Context {
    /// Current hierarchy of components leading down to what's currently being
    /// rendered
    pub current: Vec<String>,
    /// The current destination folder of where HTML artifacts should be placed.
    /// This changes as the context descends into the module hierarchy.
    pub dst: PathBuf,
    /// A flag, which when `true`, will render pages which redirect to the
    /// real location of an item. This is used to allow external links to
    /// publicly reused items to redirect to the right location.
    pub render_redirect_pages: bool,
    /// The map used to ensure all generated 'id=' attributes are unique.
    id_map: Rc<RefCell<IdMap>>,
    pub shared: Arc<SharedContext>,
    pub cache: Arc<Cache>,
}

crate struct SharedContext {
    /// The path to the crate root source minus the file name.
    /// Used for simplifying paths to the highlighted source code files.
    pub src_root: PathBuf,
    /// This describes the layout of each page, and is not modified after
    /// creation of the context (contains info like the favicon and added html).
    pub layout: layout::Layout,
    /// This flag indicates whether `[src]` links should be generated or not. If
    /// the source files are present in the html rendering, then this will be
    /// `true`.
    pub include_sources: bool,
    /// The local file sources we've emitted and their respective url-paths.
    pub local_sources: FxHashMap<PathBuf, String>,
    /// Whether the collapsed pass ran
    pub collapsed: bool,
    /// The base-URL of the issue tracker for when an item has been tagged with
    /// an issue number.
    pub issue_tracker_base_url: Option<String>,
    /// The directories that have already been created in this doc run. Used to reduce the number
    /// of spurious `create_dir_all` calls.
    pub created_dirs: RefCell<FxHashSet<PathBuf>>,
    /// This flag indicates whether listings of modules (in the side bar and documentation itself)
    /// should be ordered alphabetically or in order of appearance (in the source code).
    pub sort_modules_alphabetically: bool,
    /// Additional themes to be added to the generated docs.
    pub themes: Vec<PathBuf>,
    /// Suffix to be added on resource files (if suffix is "-v2" then "light.css" becomes
    /// "light-v2.css").
    pub resource_suffix: String,
    /// Optional path string to be used to load static files on output pages. If not set, uses
    /// combinations of `../` to reach the documentation root.
    pub static_root_path: Option<String>,
    /// Option disabled by default to generate files used by RLS and some other tools.
    pub generate_redirect_pages: bool,
    /// The fs handle we are working with.
    pub fs: DocFS,
    /// The default edition used to parse doctests.
    pub edition: Edition,
    pub codes: ErrorCodes,
    playground: Option<markdown::Playground>,
}

impl Context {
    fn path(&self, filename: &str) -> PathBuf {
        // We use splitn vs Path::extension here because we might get a filename
        // like `style.min.css` and we want to process that into
        // `style-suffix.min.css`.  Path::extension would just return `css`
        // which would result in `style.min-suffix.css` which isn't what we
        // want.
        let mut iter = filename.splitn(2, '.');
        let base = iter.next().unwrap();
        let ext = iter.next().unwrap();
        let filename = format!(
            "{}{}.{}",
            base,
            self.shared.resource_suffix,
            ext,
        );
        self.dst.join(&filename)
    }
}

impl SharedContext {
    crate fn ensure_dir(&self, dst: &Path) -> Result<(), Error> {
        let mut dirs = self.created_dirs.borrow_mut();
        if !dirs.contains(dst) {
            try_err!(self.fs.create_dir_all(dst), dst);
            dirs.insert(dst.to_path_buf());
        }

        Ok(())
    }

    /// Based on whether the `collapse-docs` pass was run, return either the `doc_value` or the
    /// `collapsed_doc_value` of the given item.
    pub fn maybe_collapsed_doc_value<'a>(&self, item: &'a clean::Item) -> Option<Cow<'a, str>> {
        if self.collapsed {
            item.collapsed_doc_value().map(|s| s.into())
        } else {
            item.doc_value().map(|s| s.into())
        }
    }
}

/// Metadata about implementations for a type or trait.
#[derive(Clone, Debug)]
pub struct Impl {
    pub impl_item: clean::Item,
}

impl Impl {
    fn inner_impl(&self) -> &clean::Impl {
        match self.impl_item.inner {
            clean::ImplItem(ref impl_) => impl_,
            _ => panic!("non-impl item found in impl")
        }
    }

    fn trait_did(&self) -> Option<DefId> {
        self.inner_impl().trait_.def_id()
    }
}

/// Temporary storage for data obtained during `RustdocVisitor::clean()`.
/// Later on moved into `CACHE_KEY`.
#[derive(Default)]
pub struct RenderInfo {
    pub inlined: FxHashSet<DefId>,
    pub external_paths: crate::core::ExternalPaths,
    pub exact_paths: FxHashMap<DefId, Vec<String>>,
    pub access_levels: AccessLevels<DefId>,
    pub deref_trait_did: Option<DefId>,
    pub deref_mut_trait_did: Option<DefId>,
    pub owned_box_did: Option<DefId>,
}

// Helper structs for rendering items/sidebars and carrying along contextual
// information

/// Struct representing one entry in the JS search index. These are all emitted
/// by hand to a large JS file at the end of cache-creation.
#[derive(Debug)]
struct IndexItem {
    ty: ItemType,
    name: String,
    path: String,
    desc: String,
    parent: Option<DefId>,
    parent_idx: Option<usize>,
    search_type: Option<IndexItemFunctionType>,
}

impl ToJson for IndexItem {
    fn to_json(&self) -> Json {
        assert_eq!(self.parent.is_some(), self.parent_idx.is_some());

        let mut data = Vec::with_capacity(6);
        data.push((self.ty as usize).to_json());
        data.push(self.name.to_json());
        data.push(self.path.to_json());
        data.push(self.desc.to_json());
        data.push(self.parent_idx.to_json());
        data.push(self.search_type.to_json());

        Json::Array(data)
    }
}

/// A type used for the search index.
#[derive(Debug)]
struct Type {
    name: Option<String>,
    generics: Option<Vec<String>>,
}

impl ToJson for Type {
    fn to_json(&self) -> Json {
        match self.name {
            Some(ref name) => {
                let mut data = Vec::with_capacity(2);
                data.push(name.to_json());
                if let Some(ref generics) = self.generics {
                    data.push(generics.to_json());
                }
                Json::Array(data)
            }
            None => Json::Null,
        }
    }
}

/// Full type of functions/methods in the search index.
#[derive(Debug)]
struct IndexItemFunctionType {
    inputs: Vec<Type>,
    output: Option<Vec<Type>>,
}

impl ToJson for IndexItemFunctionType {
    fn to_json(&self) -> Json {
        // If we couldn't figure out a type, just write `null`.
        let mut iter = self.inputs.iter();
        if match self.output {
            Some(ref output) => iter.chain(output.iter()).any(|ref i| i.name.is_none()),
            None => iter.any(|ref i| i.name.is_none()),
        } {
            Json::Null
        } else {
            let mut data = Vec::with_capacity(2);
            data.push(self.inputs.to_json());
            if let Some(ref output) = self.output {
                if output.len() > 1 {
                    data.push(output.to_json());
                } else {
                    data.push(output[0].to_json());
                }
            }
            Json::Array(data)
        }
    }
}

thread_local!(static CACHE_KEY: RefCell<Arc<Cache>> = Default::default());
thread_local!(pub static CURRENT_DEPTH: Cell<usize> = Cell::new(0));

pub fn initial_ids() -> Vec<String> {
    [
     "main",
     "search",
     "help",
     "TOC",
     "render-detail",
     "associated-types",
     "associated-const",
     "required-methods",
     "provided-methods",
     "implementors",
     "synthetic-implementors",
     "implementors-list",
     "synthetic-implementors-list",
     "methods",
     "deref-methods",
     "implementations",
    ].iter().map(|id| (String::from(*id))).collect()
}

/// Generates the documentation for `crate` into the directory `dst`
pub fn run(mut krate: clean::Crate,
           options: RenderOptions,
           renderinfo: RenderInfo,
           diag: &errors::Handler,
           edition: Edition) -> Result<(), Error> {
    // need to save a copy of the options for rendering the index page
    let md_opts = options.clone();
    let RenderOptions {
        output,
        external_html,
        id_map,
        playground_url,
        sort_modules_alphabetically,
        themes,
        extension_css,
        extern_html_root_urls,
        resource_suffix,
        static_root_path,
        generate_search_filter,
        generate_redirect_pages,
        ..
    } = options;

    let src_root = match krate.src {
        FileName::Real(ref p) => match p.parent() {
            Some(p) => p.to_path_buf(),
            None => PathBuf::new(),
        },
        _ => PathBuf::new(),
    };
    let mut errors = Arc::new(ErrorStorage::new());
    // If user passed in `--playground-url` arg, we fill in crate name here
    let mut playground = None;
    if let Some(url) = playground_url {
        playground = Some(markdown::Playground {
            crate_name: Some(krate.name.clone()),
            url,
        });
    }
    let mut layout = layout::Layout {
        logo: String::new(),
        favicon: String::new(),
        external_html,
        krate: krate.name.clone(),
        css_file_extension: extension_css,
        generate_search_filter,
    };
    let mut issue_tracker_base_url = None;
    let mut include_sources = true;

    // Crawl the crate attributes looking for attributes which control how we're
    // going to emit HTML
    if let Some(attrs) = krate.module.as_ref().map(|m| &m.attrs) {
        for attr in attrs.lists(sym::doc) {
            match (attr.name_or_empty(), attr.value_str()) {
                (sym::html_favicon_url, Some(s)) => {
                    layout.favicon = s.to_string();
                }
                (sym::html_logo_url, Some(s)) => {
                    layout.logo = s.to_string();
                }
                (sym::html_playground_url, Some(s)) => {
                    playground = Some(markdown::Playground {
                        crate_name: Some(krate.name.clone()),
                        url: s.to_string(),
                    });
                }
                (sym::issue_tracker_base_url, Some(s)) => {
                    issue_tracker_base_url = Some(s.to_string());
                }
                (sym::html_no_source, None) if attr.is_word() => {
                    include_sources = false;
                }
                _ => {}
            }
        }
    }
    let mut scx = SharedContext {
        collapsed: krate.collapsed,
        src_root,
        include_sources,
        local_sources: Default::default(),
        issue_tracker_base_url,
        layout,
        created_dirs: Default::default(),
        sort_modules_alphabetically,
        themes,
        resource_suffix,
        static_root_path,
        generate_redirect_pages,
        fs: DocFS::new(&errors),
        edition,
        codes: ErrorCodes::from(UnstableFeatures::from_environment().is_nightly_build()),
        playground,
    };

    let dst = output;
    scx.ensure_dir(&dst)?;
    krate = sources::render(&dst, &mut scx, krate)?;
    let (new_crate, index, cache) = Cache::from_krate(
        renderinfo,
        &extern_html_root_urls,
        &dst,
        krate,
    );
    krate = new_crate;
    let cache = Arc::new(cache);
    let mut cx = Context {
        current: Vec::new(),
        dst,
        render_redirect_pages: false,
        id_map: Rc::new(RefCell::new(id_map)),
        shared: Arc::new(scx),
        cache: cache.clone(),
    };

    // Freeze the cache now that the index has been built. Put an Arc into TLS
    // for future parallelization opportunities
    CACHE_KEY.with(|v| *v.borrow_mut() = cache.clone());
    CURRENT_DEPTH.with(|s| s.set(0));

    // Write shared runs within a flock; disable thread dispatching of IO temporarily.
    Arc::get_mut(&mut cx.shared).unwrap().fs.set_sync_only(true);
    write_shared(&cx, &krate, index, &md_opts, diag)?;
    Arc::get_mut(&mut cx.shared).unwrap().fs.set_sync_only(false);

    // And finally render the whole crate's documentation
    let ret = cx.krate(krate);
    let nb_errors = Arc::get_mut(&mut errors).map_or_else(|| 0, |errors| errors.write_errors(diag));
    if ret.is_err() {
        ret
    } else if nb_errors > 0 {
        Err(Error::new(io::Error::new(io::ErrorKind::Other, "I/O error"), ""))
    } else {
        Ok(())
    }
}

fn write_shared(
    cx: &Context,
    krate: &clean::Crate,
    search_index: String,
    options: &RenderOptions,
    diag: &errors::Handler,
) -> Result<(), Error> {
    // Write out the shared files. Note that these are shared among all rustdoc
    // docs placed in the output directory, so this needs to be a synchronized
    // operation with respect to all other rustdocs running around.
    let _lock = flock::Lock::panicking_new(&cx.dst.join(".lock"), true, true, true);

    // Add all the static files. These may already exist, but we just
    // overwrite them anyway to make sure that they're fresh and up-to-date.

    write_minify(&cx.shared.fs, cx.path("rustdoc.css"),
                 static_files::RUSTDOC_CSS,
                 options.enable_minification)?;
    write_minify(&cx.shared.fs, cx.path("settings.css"),
                 static_files::SETTINGS_CSS,
                 options.enable_minification)?;
    write_minify(&cx.shared.fs, cx.path("noscript.css"),
                 static_files::NOSCRIPT_CSS,
                 options.enable_minification)?;

    // To avoid "light.css" to be overwritten, we'll first run over the received themes and only
    // then we'll run over the "official" styles.
    let mut themes: FxHashSet<String> = FxHashSet::default();

    for entry in &cx.shared.themes {
        let content = try_err!(fs::read(&entry), &entry);
        let theme = try_none!(try_none!(entry.file_stem(), &entry).to_str(), &entry);
        let extension = try_none!(try_none!(entry.extension(), &entry).to_str(), &entry);
        cx.shared.fs.write(cx.path(&format!("{}.{}", theme, extension)), content.as_slice())?;
        themes.insert(theme.to_owned());
    }

    let write = |p, c| { cx.shared.fs.write(p, c) };
    if (*cx.shared).layout.logo.is_empty() {
        write(cx.path("rust-logo.png"), static_files::RUST_LOGO)?;
    }
    if (*cx.shared).layout.favicon.is_empty() {
        write(cx.path("favicon.ico"), static_files::RUST_FAVICON)?;
    }
    write(cx.path("brush.svg"), static_files::BRUSH_SVG)?;
    write(cx.path("wheel.svg"), static_files::WHEEL_SVG)?;
    write(cx.path("down-arrow.svg"), static_files::DOWN_ARROW_SVG)?;
    write_minify(&cx.shared.fs,
        cx.path("light.css"), static_files::themes::LIGHT, options.enable_minification)?;
    themes.insert("light".to_owned());
    write_minify(&cx.shared.fs,
        cx.path("dark.css"), static_files::themes::DARK, options.enable_minification)?;
    themes.insert("dark".to_owned());

    let mut themes: Vec<&String> = themes.iter().collect();
    themes.sort();
    // To avoid theme switch latencies as much as possible, we put everything theme related
    // at the beginning of the html files into another js file.
    let theme_js = format!(
r#"var themes = document.getElementById("theme-choices");
var themePicker = document.getElementById("theme-picker");

function showThemeButtonState() {{
    themes.style.display = "block";
    themePicker.style.borderBottomRightRadius = "0";
    themePicker.style.borderBottomLeftRadius = "0";
}}

function hideThemeButtonState() {{
    themes.style.display = "none";
    themePicker.style.borderBottomRightRadius = "3px";
    themePicker.style.borderBottomLeftRadius = "3px";
}}

function switchThemeButtonState() {{
    if (themes.style.display === "block") {{
        hideThemeButtonState();
    }} else {{
        showThemeButtonState();
    }}
}};

function handleThemeButtonsBlur(e) {{
    var active = document.activeElement;
    var related = e.relatedTarget;

    if (active.id !== "themePicker" &&
        (!active.parentNode || active.parentNode.id !== "theme-choices") &&
        (!related ||
         (related.id !== "themePicker" &&
          (!related.parentNode || related.parentNode.id !== "theme-choices")))) {{
        hideThemeButtonState();
    }}
}}

themePicker.onclick = switchThemeButtonState;
themePicker.onblur = handleThemeButtonsBlur;
[{}].forEach(function(item) {{
    var but = document.createElement('button');
    but.innerHTML = item;
    but.onclick = function(el) {{
        switchTheme(currentTheme, mainTheme, item, true);
    }};
    but.onblur = handleThemeButtonsBlur;
    themes.appendChild(but);
}});"#,
                 themes.iter()
                       .map(|s| format!("\"{}\"", s))
                       .collect::<Vec<String>>()
                       .join(","));
    write(cx.dst.join(&format!("theme{}.js", cx.shared.resource_suffix)),
          theme_js.as_bytes()
    )?;

    write_minify(&cx.shared.fs, cx.path("main.js"),
                 static_files::MAIN_JS,
                 options.enable_minification)?;
    write_minify(&cx.shared.fs, cx.path("settings.js"),
                 static_files::SETTINGS_JS,
                 options.enable_minification)?;
    if cx.shared.include_sources {
        write_minify(
            &cx.shared.fs,
            cx.path("source-script.js"),
            static_files::sidebar::SOURCE_SCRIPT,
            options.enable_minification)?;
    }

    {
        write_minify(
            &cx.shared.fs,
            cx.path("storage.js"),
            &format!("var resourcesSuffix = \"{}\";{}",
                     cx.shared.resource_suffix,
                     static_files::STORAGE_JS),
            options.enable_minification)?;
    }

    if let Some(ref css) = cx.shared.layout.css_file_extension {
        let out = cx.path("theme.css");
        let buffer = try_err!(fs::read_to_string(css), css);
        if !options.enable_minification {
            cx.shared.fs.write(&out, &buffer)?;
        } else {
            write_minify(&cx.shared.fs, out, &buffer, options.enable_minification)?;
        }
    }
    write_minify(&cx.shared.fs, cx.path("normalize.css"),
                 static_files::NORMALIZE_CSS,
                 options.enable_minification)?;
    write(cx.dst.join("FiraSans-Regular.woff"),
          static_files::fira_sans::REGULAR)?;
    write(cx.dst.join("FiraSans-Medium.woff"),
          static_files::fira_sans::MEDIUM)?;
    write(cx.dst.join("FiraSans-LICENSE.txt"),
          static_files::fira_sans::LICENSE)?;
    write(cx.dst.join("SourceSerifPro-Regular.ttf.woff"),
          static_files::source_serif_pro::REGULAR)?;
    write(cx.dst.join("SourceSerifPro-Bold.ttf.woff"),
          static_files::source_serif_pro::BOLD)?;
    write(cx.dst.join("SourceSerifPro-It.ttf.woff"),
          static_files::source_serif_pro::ITALIC)?;
    write(cx.dst.join("SourceSerifPro-LICENSE.md"),
          static_files::source_serif_pro::LICENSE)?;
    write(cx.dst.join("SourceCodePro-Regular.woff"),
          static_files::source_code_pro::REGULAR)?;
    write(cx.dst.join("SourceCodePro-Semibold.woff"),
          static_files::source_code_pro::SEMIBOLD)?;
    write(cx.dst.join("SourceCodePro-LICENSE.txt"),
          static_files::source_code_pro::LICENSE)?;
    write(cx.dst.join("LICENSE-MIT.txt"),
          static_files::LICENSE_MIT)?;
    write(cx.dst.join("LICENSE-APACHE.txt"),
          static_files::LICENSE_APACHE)?;
    write(cx.dst.join("COPYRIGHT.txt"),
          static_files::COPYRIGHT)?;

    fn collect(
        path: &Path,
        krate: &str,
        key: &str,
        for_search_index: bool,
    ) -> io::Result<(Vec<String>, Vec<String>, Vec<String>)> {
        let mut ret = Vec::new();
        let mut krates = Vec::new();
        let mut variables = Vec::new();

        if path.exists() {
            for line in BufReader::new(File::open(path)?).lines() {
                let line = line?;
                if for_search_index && line.starts_with("var R") {
                    variables.push(line.clone());
                    continue;
                }
                if !line.starts_with(key) {
                    continue;
                }
                if line.starts_with(&format!(r#"{}["{}"]"#, key, krate)) {
                    continue;
                }
                ret.push(line.to_string());
                krates.push(line[key.len() + 2..].split('"')
                                                 .next()
                                                 .map(|s| s.to_owned())
                                                 .unwrap_or_else(|| String::new()));
            }
        }
        Ok((ret, krates, variables))
    }

    fn show_item(item: &IndexItem, krate: &str) -> String {
        format!("{{'crate':'{}','ty':{},'name':'{}','desc':'{}','p':'{}'{}}}",
                krate, item.ty as usize, item.name, item.desc.replace("'", "\\'"), item.path,
                if let Some(p) = item.parent_idx {
                    format!(",'parent':{}", p)
                } else {
                    String::new()
                })
    }

    let dst = cx.dst.join(&format!("aliases{}.js", cx.shared.resource_suffix));
    {
        let (mut all_aliases, _, _) = try_err!(collect(&dst, &krate.name, "ALIASES", false), &dst);
        let mut output = String::with_capacity(100);
        for (alias, items) in &cx.cache.aliases {
            if items.is_empty() {
                continue
            }
            output.push_str(&format!("\"{}\":[{}],",
                                     alias,
                                     items.iter()
                                          .map(|v| show_item(v, &krate.name))
                                          .collect::<Vec<_>>()
                                          .join(",")));
        }
        all_aliases.push(format!("ALIASES[\"{}\"] = {{{}}};", krate.name, output));
        all_aliases.sort();
        let mut v = Buffer::html();
        writeln!(&mut v, "var ALIASES = {{}};");
        for aliases in &all_aliases {
            writeln!(&mut v, "{}", aliases);
        }
        cx.shared.fs.write(&dst, v.into_inner().into_bytes())?;
    }

    use std::ffi::OsString;

    #[derive(Debug)]
    struct Hierarchy {
        elem: OsString,
        children: FxHashMap<OsString, Hierarchy>,
        elems: FxHashSet<OsString>,
    }

    impl Hierarchy {
        fn new(elem: OsString) -> Hierarchy {
            Hierarchy {
                elem,
                children: FxHashMap::default(),
                elems: FxHashSet::default(),
            }
        }

        fn to_json_string(&self) -> String {
            let mut subs: Vec<&Hierarchy> = self.children.values().collect();
            subs.sort_unstable_by(|a, b| a.elem.cmp(&b.elem));
            let mut files = self.elems.iter()
                                      .map(|s| format!("\"{}\"",
                                                       s.to_str()
                                                        .expect("invalid osstring conversion")))
                                      .collect::<Vec<_>>();
            files.sort_unstable_by(|a, b| a.cmp(b));
            let subs = subs.iter().map(|s| s.to_json_string()).collect::<Vec<_>>().join(",");
            let dirs = if subs.is_empty() {
                String::new()
            } else {
                format!(",\"dirs\":[{}]", subs)
            };
            let files = files.join(",");
            let files = if files.is_empty() {
                String::new()
            } else {
                format!(",\"files\":[{}]", files)
            };
            format!("{{\"name\":\"{name}\"{dirs}{files}}}",
                    name=self.elem.to_str().expect("invalid osstring conversion"),
                    dirs=dirs,
                    files=files)
        }
    }

    if cx.shared.include_sources {
        let mut hierarchy = Hierarchy::new(OsString::new());
        for source in cx.shared.local_sources.iter()
                                             .filter_map(|p| p.0.strip_prefix(&cx.shared.src_root)
                                                                .ok()) {
            let mut h = &mut hierarchy;
            let mut elems = source.components()
                                  .filter_map(|s| {
                                      match s {
                                          Component::Normal(s) => Some(s.to_owned()),
                                          _ => None,
                                      }
                                  })
                                  .peekable();
            loop {
                let cur_elem = elems.next().expect("empty file path");
                if elems.peek().is_none() {
                    h.elems.insert(cur_elem);
                    break;
                } else {
                    let e = cur_elem.clone();
                    h.children.entry(cur_elem.clone()).or_insert_with(|| Hierarchy::new(e));
                    h = h.children.get_mut(&cur_elem).expect("not found child");
                }
            }
        }

        let dst = cx.dst.join(&format!("source-files{}.js", cx.shared.resource_suffix));
        let (mut all_sources, _krates, _) = try_err!(collect(&dst, &krate.name, "sourcesIndex",
                                                             false),
                                                     &dst);
        all_sources.push(format!("sourcesIndex[\"{}\"] = {};",
                                 &krate.name,
                                 hierarchy.to_json_string()));
        all_sources.sort();
        let v = format!("var N = null;var sourcesIndex = {{}};\n{}\ncreateSourceSidebar();\n",
                          all_sources.join("\n"));
        cx.shared.fs.write(&dst, v.as_bytes())?;
    }

    // Update the search index
    let dst = cx.dst.join(&format!("search-index{}.js", cx.shared.resource_suffix));
    let (mut all_indexes, mut krates, variables) = try_err!(collect(&dst,
                                                                    &krate.name,
                                                                    "searchIndex",
                                                                    true), &dst);
    all_indexes.push(search_index);

    // Sort the indexes by crate so the file will be generated identically even
    // with rustdoc running in parallel.
    all_indexes.sort();
    {
        let mut v = String::from("var N=null,E=\"\",T=\"t\",U=\"u\",searchIndex={};\n");
        v.push_str(&minify_replacer(
            &format!("{}\n{}", variables.join(""), all_indexes.join("\n")),
            options.enable_minification));
        v.push_str("initSearch(searchIndex);addSearchOptions(searchIndex);");
        cx.shared.fs.write(&dst, &v)?;
    }
    if options.enable_index_page {
        if let Some(index_page) = options.index_page.clone() {
            let mut md_opts = options.clone();
            md_opts.output = cx.dst.clone();
            md_opts.external_html = (*cx.shared).layout.external_html.clone();

            crate::markdown::render(index_page, md_opts, diag, cx.shared.edition);
        } else {
            let dst = cx.dst.join("index.html");
            let page = layout::Page {
                title: "Index of crates",
                css_class: "mod",
                root_path: "./",
                static_root_path: cx.shared.static_root_path.as_deref(),
                description: "List of crates",
                keywords: BASIC_KEYWORDS,
                resource_suffix: &cx.shared.resource_suffix,
                extra_scripts: &[],
                static_extra_scripts: &[],
            };
            krates.push(krate.name.clone());
            krates.sort();
            krates.dedup();

            let content = format!(
"<h1 class='fqn'>\
     <span class='in-band'>List of all crates</span>\
</h1><ul class='mod'>{}</ul>",
                                  krates
                                    .iter()
                                    .map(|s| {
                                        format!("<li><a href=\"{}index.html\">{}</li>",
                                                ensure_trailing_slash(s), s)
                                    })
                                    .collect::<String>());
            let v = layout::render(&cx.shared.layout,
                           &page, "", content,
                           &cx.shared.themes);
            cx.shared.fs.write(&dst, v.as_bytes())?;
        }
    }

    // Update the list of all implementors for traits
    let dst = cx.dst.join("implementors");
    for (&did, imps) in &cx.cache.implementors {
        // Private modules can leak through to this phase of rustdoc, which
        // could contain implementations for otherwise private types. In some
        // rare cases we could find an implementation for an item which wasn't
        // indexed, so we just skip this step in that case.
        //
        // FIXME: this is a vague explanation for why this can't be a `get`, in
        //        theory it should be...
        let &(ref remote_path, remote_item_type) = match cx.cache.paths.get(&did) {
            Some(p) => p,
            None => match cx.cache.external_paths.get(&did) {
                Some(p) => p,
                None => continue,
            }
        };

        let mut have_impls = false;
        let mut implementors = format!(r#"implementors["{}"] = ["#, krate.name);
        for imp in imps {
            // If the trait and implementation are in the same crate, then
            // there's no need to emit information about it (there's inlining
            // going on). If they're in different crates then the crate defining
            // the trait will be interested in our implementation.
            if imp.impl_item.def_id.krate == did.krate { continue }
            // If the implementation is from another crate then that crate
            // should add it.
            if !imp.impl_item.def_id.is_local() { continue }
            have_impls = true;
            write!(implementors, "{{text:{},synthetic:{},types:{}}},",
                   as_json(&imp.inner_impl().print().to_string()),
                   imp.inner_impl().synthetic,
                   as_json(&collect_paths_for_type(imp.inner_impl().for_.clone()))).unwrap();
        }
        implementors.push_str("];");

        // Only create a js file if we have impls to add to it. If the trait is
        // documented locally though we always create the file to avoid dead
        // links.
        if !have_impls && !cx.cache.paths.contains_key(&did) {
            continue;
        }

        let mut mydst = dst.clone();
        for part in &remote_path[..remote_path.len() - 1] {
            mydst.push(part);
        }
        cx.shared.ensure_dir(&mydst)?;
        mydst.push(&format!("{}.{}.js",
                            remote_item_type,
                            remote_path[remote_path.len() - 1]));

        let (mut all_implementors, _, _) = try_err!(collect(&mydst, &krate.name, "implementors",
                                                            false),
                                                    &mydst);
        all_implementors.push(implementors);
        // Sort the implementors by crate so the file will be generated
        // identically even with rustdoc running in parallel.
        all_implementors.sort();

        let mut v = String::from("(function() {var implementors = {};\n");
        for implementor in &all_implementors {
            writeln!(v, "{}", *implementor).unwrap();
        }
        v.push_str(r"
            if (window.register_implementors) {
                window.register_implementors(implementors);
            } else {
                window.pending_implementors = implementors;
            }
        ");
        v.push_str("})()");
        cx.shared.fs.write(&mydst, &v)?;
    }
    Ok(())
}

fn write_minify(fs:&DocFS, dst: PathBuf, contents: &str, enable_minification: bool
                ) -> Result<(), Error> {
    if enable_minification {
        if dst.extension() == Some(&OsStr::new("css")) {
            let res = try_none!(minifier::css::minify(contents).ok(), &dst);
            fs.write(dst, res.as_bytes())
        } else {
            fs.write(dst, minifier::js::minify(contents).as_bytes())
        }
    } else {
        fs.write(dst, contents.as_bytes())
    }
}

fn minify_replacer(
    contents: &str,
    enable_minification: bool,
) -> String {
    use minifier::js::{simple_minify, Keyword, ReservedChar, Token, Tokens};

    if enable_minification {
        let tokens: Tokens<'_> = simple_minify(contents)
            .into_iter()
            .filter(|(f, next)| {
                // We keep backlines.
                minifier::js::clean_token_except(f, next, &|c: &Token<'_>| {
                    c.get_char() != Some(ReservedChar::Backline)
                })
            })
            .map(|(f, _)| {
                minifier::js::replace_token_with(f, &|t: &Token<'_>| {
                    match *t {
                        Token::Keyword(Keyword::Null) => Some(Token::Other("N")),
                        Token::String(s) => {
                            let s = &s[1..s.len() -1]; // The quotes are included
                            if s.is_empty() {
                                Some(Token::Other("E"))
                            } else if s == "t" {
                                Some(Token::Other("T"))
                            } else if s == "u" {
                                Some(Token::Other("U"))
                            } else {
                                None
                            }
                        }
                        _ => None,
                    }
                })
            })
            .collect::<Vec<_>>()
            .into();
        let o = tokens.apply(|f| {
            // We add a backline after the newly created variables.
            minifier::js::aggregate_strings_into_array_with_separation_filter(
                f,
                "R",
                Token::Char(ReservedChar::Backline),
                // This closure prevents crates' names from being aggregated.
                //
                // The point here is to check if the string is preceded by '[' and
                // "searchIndex". If so, it means this is a crate name and that it
                // shouldn't be aggregated.
                |tokens, pos| {
                    pos < 2 ||
                    !tokens[pos - 1].eq_char(ReservedChar::OpenBracket) ||
                    tokens[pos - 2].get_other() != Some("searchIndex")
                }
            )
        })
        .to_string();
        format!("{}\n", o)
    } else {
        format!("{}\n", contents)
    }
}

#[derive(Debug, Eq, PartialEq, Hash)]
struct ItemEntry {
    url: String,
    name: String,
}

impl ItemEntry {
    fn new(mut url: String, name: String) -> ItemEntry {
        while url.starts_with('/') {
            url.remove(0);
        }
        ItemEntry {
            url,
            name,
        }
    }
}

impl ItemEntry {
    crate fn print(&self) -> impl fmt::Display + '_ {
        crate::html::format::display_fn(move |f| {
            write!(f, "<a href='{}'>{}</a>", self.url, Escape(&self.name))
        })
    }
}

impl PartialOrd for ItemEntry {
    fn partial_cmp(&self, other: &ItemEntry) -> Option<::std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ItemEntry {
    fn cmp(&self, other: &ItemEntry) -> ::std::cmp::Ordering {
        self.name.cmp(&other.name)
    }
}

#[derive(Debug)]
struct AllTypes {
    structs: FxHashSet<ItemEntry>,
    enums: FxHashSet<ItemEntry>,
    unions: FxHashSet<ItemEntry>,
    primitives: FxHashSet<ItemEntry>,
    traits: FxHashSet<ItemEntry>,
    macros: FxHashSet<ItemEntry>,
    functions: FxHashSet<ItemEntry>,
    typedefs: FxHashSet<ItemEntry>,
    opaque_tys: FxHashSet<ItemEntry>,
    statics: FxHashSet<ItemEntry>,
    constants: FxHashSet<ItemEntry>,
    keywords: FxHashSet<ItemEntry>,
    attributes: FxHashSet<ItemEntry>,
    derives: FxHashSet<ItemEntry>,
    trait_aliases: FxHashSet<ItemEntry>,
}

impl AllTypes {
    fn new() -> AllTypes {
        let new_set = |cap| FxHashSet::with_capacity_and_hasher(cap, Default::default());
        AllTypes {
            structs: new_set(100),
            enums: new_set(100),
            unions: new_set(100),
            primitives: new_set(26),
            traits: new_set(100),
            macros: new_set(100),
            functions: new_set(100),
            typedefs: new_set(100),
            opaque_tys: new_set(100),
            statics: new_set(100),
            constants: new_set(100),
            keywords: new_set(100),
            attributes: new_set(100),
            derives: new_set(100),
            trait_aliases: new_set(100),
        }
    }

    fn append(&mut self, item_name: String, item_type: &ItemType) {
        let mut url: Vec<_> = item_name.split("::").skip(1).collect();
        if let Some(name) = url.pop() {
            let new_url = format!("{}/{}.{}.html", url.join("/"), item_type, name);
            url.push(name);
            let name = url.join("::");
            match *item_type {
                ItemType::Struct => self.structs.insert(ItemEntry::new(new_url, name)),
                ItemType::Enum => self.enums.insert(ItemEntry::new(new_url, name)),
                ItemType::Union => self.unions.insert(ItemEntry::new(new_url, name)),
                ItemType::Primitive => self.primitives.insert(ItemEntry::new(new_url, name)),
                ItemType::Trait => self.traits.insert(ItemEntry::new(new_url, name)),
                ItemType::Macro => self.macros.insert(ItemEntry::new(new_url, name)),
                ItemType::Function => self.functions.insert(ItemEntry::new(new_url, name)),
                ItemType::Typedef => self.typedefs.insert(ItemEntry::new(new_url, name)),
                ItemType::OpaqueTy => self.opaque_tys.insert(ItemEntry::new(new_url, name)),
                ItemType::Static => self.statics.insert(ItemEntry::new(new_url, name)),
                ItemType::Constant => self.constants.insert(ItemEntry::new(new_url, name)),
                ItemType::ProcAttribute => self.attributes.insert(ItemEntry::new(new_url, name)),
                ItemType::ProcDerive => self.derives.insert(ItemEntry::new(new_url, name)),
                ItemType::TraitAlias => self.trait_aliases.insert(ItemEntry::new(new_url, name)),
                _ => true,
            };
        }
    }
}

fn print_entries(f: &mut Buffer, e: &FxHashSet<ItemEntry>, title: &str, class: &str) {
    if !e.is_empty() {
        let mut e: Vec<&ItemEntry> = e.iter().collect();
        e.sort();
        write!(f, "<h3 id='{}'>{}</h3><ul class='{} docblock'>{}</ul>",
               title,
               Escape(title),
               class,
               e.iter().map(|s| format!("<li>{}</li>", s.print())).collect::<String>());
    }
}

impl AllTypes {
    fn print(self, f: &mut Buffer) {
        write!(f,
    "<h1 class='fqn'>\
        <span class='out-of-band'>\
            <span id='render-detail'>\
                <a id=\"toggle-all-docs\" href=\"javascript:void(0)\" title=\"collapse all docs\">\
                    [<span class='inner'>&#x2212;</span>]\
                </a>\
            </span>
        </span>
        <span class='in-band'>List of all items</span>\
    </h1>");
        print_entries(f, &self.structs, "Structs", "structs");
        print_entries(f, &self.enums, "Enums", "enums");
        print_entries(f, &self.unions, "Unions", "unions");
        print_entries(f, &self.primitives, "Primitives", "primitives");
        print_entries(f, &self.traits, "Traits", "traits");
        print_entries(f, &self.macros, "Macros", "macros");
        print_entries(f, &self.attributes, "Attribute Macros", "attributes");
        print_entries(f, &self.derives, "Derive Macros", "derives");
        print_entries(f, &self.functions, "Functions", "functions");
        print_entries(f, &self.typedefs, "Typedefs", "typedefs");
        print_entries(f, &self.trait_aliases, "Trait Aliases", "trait-aliases");
        print_entries(f, &self.opaque_tys, "Opaque Types", "opaque-types");
        print_entries(f, &self.statics, "Statics", "statics");
        print_entries(f, &self.constants, "Constants", "constants")
    }
}

#[derive(Debug)]
enum Setting {
    Section {
        description: &'static str,
        sub_settings: Vec<Setting>,
    },
    Entry {
        js_data_name: &'static str,
        description: &'static str,
        default_value: bool,
    }
}

impl Setting {
    fn display(&self) -> String {
        match *self {
            Setting::Section { ref description, ref sub_settings } => {
                format!(
                    "<div class='setting-line'>\
                        <div class='title'>{}</div>\
                        <div class='sub-settings'>{}</div>
                    </div>",
                    description,
                    sub_settings.iter().map(|s| s.display()).collect::<String>()
                )
            }
            Setting::Entry { ref js_data_name, ref description, ref default_value } => {
                format!(
                    "<div class='setting-line'>\
                        <label class='toggle'>\
                        <input type='checkbox' id='{}' {}>\
                        <span class='slider'></span>\
                        </label>\
                        <div>{}</div>\
                    </div>",
                    js_data_name,
                    if *default_value { " checked" } else { "" },
                    description,
                )
            }
        }
    }
}

impl From<(&'static str, &'static str, bool)> for Setting {
    fn from(values: (&'static str, &'static str, bool)) -> Setting {
        Setting::Entry {
            js_data_name: values.0,
            description: values.1,
            default_value: values.2,
        }
    }
}

impl<T: Into<Setting>> From<(&'static str, Vec<T>)> for Setting {
    fn from(values: (&'static str, Vec<T>)) -> Setting {
        Setting::Section {
            description: values.0,
            sub_settings: values.1.into_iter().map(|v| v.into()).collect::<Vec<_>>(),
        }
    }
}

fn settings(root_path: &str, suffix: &str) -> String {
    // (id, explanation, default value)
    let settings: &[Setting] = &[
        ("Auto-hide item declarations", vec![
            ("auto-hide-struct", "Auto-hide structs declaration", true),
            ("auto-hide-enum", "Auto-hide enums declaration", false),
            ("auto-hide-union", "Auto-hide unions declaration", true),
            ("auto-hide-trait", "Auto-hide traits declaration", true),
            ("auto-hide-macro", "Auto-hide macros declaration", false),
        ]).into(),
        ("auto-hide-attributes", "Auto-hide item attributes.", true).into(),
        ("auto-hide-method-docs", "Auto-hide item methods' documentation", false).into(),
        ("auto-hide-trait-implementations", "Auto-hide trait implementations documentation",
            true).into(),
        ("go-to-only-result", "Directly go to item in search if there is only one result",
            false).into(),
        ("line-numbers", "Show line numbers on code examples", false).into(),
        ("disable-shortcuts", "Disable keyboard shortcuts", false).into(),
    ];
    format!(
"<h1 class='fqn'>\
    <span class='in-band'>Rustdoc settings</span>\
</h1>\
<div class='settings'>{}</div>\
<script src='{}settings{}.js'></script>",
            settings.iter().map(|s| s.display()).collect::<String>(),
            root_path,
            suffix)
}

impl Context {
    fn derive_id(&self, id: String) -> String {
        let mut map = self.id_map.borrow_mut();
        map.derive(id)
    }

    /// String representation of how to get back to the root path of the 'doc/'
    /// folder in terms of a relative URL.
    fn root_path(&self) -> String {
        "../".repeat(self.current.len())
    }

    /// Main method for rendering a crate.
    ///
    /// This currently isn't parallelized, but it'd be pretty easy to add
    /// parallelization to this function.
    fn krate(self, mut krate: clean::Crate) -> Result<(), Error> {
        let mut item = match krate.module.take() {
            Some(i) => i,
            None => return Ok(()),
        };
        let final_file = self.dst.join(&krate.name)
                                 .join("all.html");
        let settings_file = self.dst.join("settings.html");

        let crate_name = krate.name.clone();
        item.name = Some(krate.name);

        let mut all = AllTypes::new();

        {
            // Render the crate documentation
            let mut work = vec![(self.clone(), item)];

            while let Some((mut cx, item)) = work.pop() {
                cx.item(item, &mut all, |cx, item| {
                    work.push((cx.clone(), item))
                })?
            }
        }

        let mut root_path = self.dst.to_str().expect("invalid path").to_owned();
        if !root_path.ends_with('/') {
            root_path.push('/');
        }
        let mut page = layout::Page {
            title: "List of all items in this crate",
            css_class: "mod",
            root_path: "../",
            static_root_path: self.shared.static_root_path.as_deref(),
            description: "List of all items in this crate",
            keywords: BASIC_KEYWORDS,
            resource_suffix: &self.shared.resource_suffix,
            extra_scripts: &[],
            static_extra_scripts: &[],
        };
        let sidebar = if let Some(ref version) = self.cache.crate_version {
            format!("<p class='location'>Crate {}</p>\
                     <div class='block version'>\
                         <p>Version {}</p>\
                     </div>\
                     <a id='all-types' href='index.html'><p>Back to index</p></a>",
                    crate_name, version)
        } else {
            String::new()
        };
        let v = layout::render(&self.shared.layout,
                       &page, sidebar, |buf: &mut Buffer| all.print(buf),
                       &self.shared.themes);
        self.shared.fs.write(&final_file, v.as_bytes())?;

        // Generating settings page.
        page.title = "Rustdoc settings";
        page.description = "Settings of Rustdoc";
        page.root_path = "./";

        let mut themes = self.shared.themes.clone();
        let sidebar = "<p class='location'>Settings</p><div class='sidebar-elems'></div>";
        themes.push(PathBuf::from("settings.css"));
        let v = layout::render(
            &self.shared.layout,
            &page, sidebar, settings(
                self.shared.static_root_path.as_deref().unwrap_or("./"),
                &self.shared.resource_suffix
            ),
            &themes);
        self.shared.fs.write(&settings_file, v.as_bytes())?;

        Ok(())
    }

    fn render_item(&self,
                   it: &clean::Item,
                   pushname: bool) -> String {
        // A little unfortunate that this is done like this, but it sure
        // does make formatting *a lot* nicer.
        CURRENT_DEPTH.with(|slot| {
            slot.set(self.current.len());
        });

        let mut title = if it.is_primitive() || it.is_keyword() {
            // No need to include the namespace for primitive types and keywords
            String::new()
        } else {
            self.current.join("::")
        };
        if pushname {
            if !title.is_empty() {
                title.push_str("::");
            }
            title.push_str(it.name.as_ref().unwrap());
        }
        title.push_str(" - Rust");
        let tyname = it.type_();
        let desc = if it.is_crate() {
            format!("API documentation for the Rust `{}` crate.",
                    self.shared.layout.krate)
        } else {
            format!("API documentation for the Rust `{}` {} in crate `{}`.",
                    it.name.as_ref().unwrap(), tyname, self.shared.layout.krate)
        };
        let keywords = make_item_keywords(it);
        let page = layout::Page {
            css_class: tyname.as_str(),
            root_path: &self.root_path(),
            static_root_path: self.shared.static_root_path.as_deref(),
            title: &title,
            description: &desc,
            keywords: &keywords,
            resource_suffix: &self.shared.resource_suffix,
            extra_scripts: &[],
            static_extra_scripts: &[],
        };

        {
            self.id_map.borrow_mut().reset();
            self.id_map.borrow_mut().populate(initial_ids());
        }

        if !self.render_redirect_pages {
            layout::render(&self.shared.layout, &page,
                           |buf: &mut _| print_sidebar(self, it, buf),
                           |buf: &mut _| print_item(self, it, buf),
                           &self.shared.themes)
        } else {
            let mut url = self.root_path();
            if let Some(&(ref names, ty)) = self.cache.paths.get(&it.def_id) {
                for name in &names[..names.len() - 1] {
                    url.push_str(name);
                    url.push_str("/");
                }
                url.push_str(&item_path(ty, names.last().unwrap()));
                layout::redirect(&url)
            } else {
                String::new()
            }
        }
    }

    /// Non-parallelized version of rendering an item. This will take the input
    /// item, render its contents, and then invoke the specified closure with
    /// all sub-items which need to be rendered.
    ///
    /// The rendering driver uses this closure to queue up more work.
    fn item<F>(&mut self, item: clean::Item, all: &mut AllTypes, mut f: F) -> Result<(), Error>
        where F: FnMut(&mut Context, clean::Item),
    {
        // Stripped modules survive the rustdoc passes (i.e., `strip-private`)
        // if they contain impls for public types. These modules can also
        // contain items such as publicly re-exported structures.
        //
        // External crates will provide links to these structures, so
        // these modules are recursed into, but not rendered normally
        // (a flag on the context).
        if !self.render_redirect_pages {
            self.render_redirect_pages = item.is_stripped();
        }

        if item.is_mod() {
            // modules are special because they add a namespace. We also need to
            // recurse into the items of the module as well.
            let name = item.name.as_ref().unwrap().to_string();
            let scx = &self.shared;
            if name.is_empty() {
                panic!("Unexpected empty destination: {:?}", self.current);
            }
            let prev = self.dst.clone();
            self.dst.push(&name);
            self.current.push(name);

            info!("Recursing into {}", self.dst.display());

            let buf = self.render_item(&item, false);
            // buf will be empty if the module is stripped and there is no redirect for it
            if !buf.is_empty() {
                self.shared.ensure_dir(&self.dst)?;
                let joint_dst = self.dst.join("index.html");
                scx.fs.write(&joint_dst, buf.as_bytes())?;
            }

            let m = match item.inner {
                clean::StrippedItem(box clean::ModuleItem(m)) |
                clean::ModuleItem(m) => m,
                _ => unreachable!()
            };

            // Render sidebar-items.js used throughout this module.
            if !self.render_redirect_pages {
                let items = self.build_sidebar_items(&m);
                let js_dst = self.dst.join("sidebar-items.js");
                let v = format!("initSidebarItems({});", as_json(&items));
                scx.fs.write(&js_dst, &v)?;
            }

            for item in m.items {
                f(self, item);
            }

            info!("Recursed; leaving {}", self.dst.display());

            // Go back to where we were at
            self.dst = prev;
            self.current.pop().unwrap();
        } else if item.name.is_some() {
            let buf = self.render_item(&item, true);
            // buf will be empty if the item is stripped and there is no redirect for it
            if !buf.is_empty() {
                let name = item.name.as_ref().unwrap();
                let item_type = item.type_();
                let file_name = &item_path(item_type, name);
                self.shared.ensure_dir(&self.dst)?;
                let joint_dst = self.dst.join(file_name);
                self.shared.fs.write(&joint_dst, buf.as_bytes())?;

                if !self.render_redirect_pages {
                    all.append(full_path(self, &item), &item_type);
                }
                if self.shared.generate_redirect_pages {
                    // Redirect from a sane URL using the namespace to Rustdoc's
                    // URL for the page.
                    let redir_name = format!("{}.{}.html", name, item_type.name_space());
                    let redir_dst = self.dst.join(redir_name);
                    let v = layout::redirect(file_name);
                    self.shared.fs.write(&redir_dst