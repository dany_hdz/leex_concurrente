extern mod vec_util;

fn fibo(f: &fn(&uint) -> bool) {
  let mut (a, b) = (1,1);
  loop {
    if ! f(&a) { break }
    let c = a;
    a = a+b;
    b = c;
  }
}

fn main() {
  let mut sum: uint = 0;
  let f : &fn(&uint) -> bool = |n: &uint| {
    if *n>4000000 { false }
    else { sum = sum + *n; true }
  };
  fibo(f);
  io::println(sum.to_str());
}