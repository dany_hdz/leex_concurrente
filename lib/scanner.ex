defmodule Scanner do
  def benchmark(folder \\ "rust") do
    Benchee.run(%{
      "lin" => fn -> lineal(folder) end,
      "conc" => fn -> concurrente(folder) end
      })
  end

  def lineal(folder) do
    {:ok, files} = File.ls(folder)
    Enum.map(files, fn file -> scan(folder, file) end)
  end

  def concurrente(folder) do
    {:ok, files} = File.ls(folder)
    Enum.map(files, fn file -> Task.async(fn -> scan(folder, file) end) end)
  end

  def scan(folder, file) do
    html = "<!DOCTYPE html><html lang=\"en \"><head>
        <meta charset=\"UTF-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <title>Highlighter</title>
        <style>
            body {background-color: #202124; color: white; font-family: 'Courier New'; margin-left: 50px;}
            .keyword {color: #44CCE5;}
            .datatype {color: royalblue;}
            .default {color: white;}
            .number {color: #ED7303;}
            .string {color: #4ed190;}
            .comment {color: grey;}
            .operator {color: red;}
            .macro {color: #fadd3c;}
            .function {color: #A06DC0}
        </style></head><body><h1>Resaltador l&eacute;xico de Rust</h1><pre><code>"

    File.read!("#{folder}/#{file}")
    |> String.to_charlist
    |> :lexer.string
    |> elem(1)
    |> token(html, file)
  end

  def token([], code, file) do
    build_file(code, file)
  end

  def token([{:keyword,_,word}|t], code, file) do
    line = "<span class=\"keyword\">" <> to_string(word) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:datatype,_,number}|t], code, file) do
    line = "<span class=\"datatype\">" <> to_string(number) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:int,_,number}|t], code, file) do
    line = "<span class=\"number\">" <> to_string(number) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:hex,_,number}|t], code, file) do
    line = "<span class=\"number\">" <> to_string(number) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:octal,_,number}|t], code, file) do
    line = "<span class=\"number\">" <> to_string(number) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:bin,_,number}|t], code, file) do
    line = "<span class=\"number\">" <> to_string(number) <> "</span>"
    token(t, code <> line,  file)
  end

  def token([{:float,_,number}|t], code, file) do
    line = "<span class=\"number\">" <> to_string(number) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:char,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:string,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:rawString,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:byte,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:byteString,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:rawByteString,_,word}|t], code, file) do
    text = word |> to_string() |> HtmlEntities.encode
    line = "<span class=\"string\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:space,_,space}|t], code, file) do
    token(t, code <> to_string(space), file)
  end

  def token([{:identifier,_,word}|t], code, file) do
    line = "<span class=\"default\">" <> to_string(word) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:test,_,word}|t], code, file) do
    line = "<span class=\"default\">" <> to_string(word) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:lineComment,_,comment}|t], code, file) do
    text = comment |> to_string() |> HtmlEntities.encode
    line = "<span class=\"comment\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:blockComment,_,comment}|t], code, file) do
    text = comment |> to_string() |> HtmlEntities.encode
    line = "<span class=\"comment\">" <> text <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:operator,_,token}|t], code, file) do
    line = "<span class=\"operator\">" <> to_string(token) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:delimiter,_,token}|t], code, file) do
    line = "<span class=\"default\">" <> to_string(token) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:macro,_,word}|t], code, file) do
    line = "<span class=\"macro\">" <> to_string(word) <> "</span>"
    token(t, code <> line, file)
  end

  def token([{:function,_,word}|t], code, file) do
    line = "<span class=\"function\">" <> String.slice(to_string(word), 0..-2) <> "</span>" <> "<span>" <> String.slice(to_string(word), -1..-1) <> "</span>"
    token(t, code <> line, file)
  end

  def build_file(code, file) do
    filename = String.slice(file, 0..-3) <> "html"
    File.open!("html/#{filename}", [:write], fn file ->
      IO.puts(file, code)
      IO.puts(file, "</pre></code></body></html>")
    end)
  end

end
